# Test the template
import sys
sys.path.append('../')
import logging
import layer4.stub_layer_4 as Layer4
logging.basicConfig(level=logging.DEBUG)

# Start Echo Server
def echo_server_receive(data):
    logging.debug("Echo server received {data}") 
    echo_layer4.from_layer_5(data=data, src_port=None,
            dest_port=None, dest_addr=None)

echo_layer4=Layer4.StubLayer4()
echo_layer4.connect_to_socket(80, echo_server_receive)
logging.info("Echo server online.")


