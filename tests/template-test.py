# Test the template
import sys
sys.path.append('../')
import logging
import layer4.stub_layer_4 as Layer4
logging.basicConfig(level=logging.DEBUG)

# Send message

test_message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt."

def mismatches(a, b):
    """Assumes a and b have the same number of elements"""
    matching_chars = [x for x in zip(a,b) if x[0]!=x[1]]
    return len(matching_chars)

def test_server_receive(data):
    logging.info(f"Test server received {data}") 
    error_rate = mismatches(test_message, data) / len(test_message)
    logging.info(f"Error rate: {error_rate}")

test_layer4 = Layer4.StubLayer4()
test_layer4.connect_to_socket(90, test_server_receive)

test_layer4.from_layer_5(data=test_message, src_port=None,
            dest_port=None, dest_addr=None)


